import csv

# Tratamento de exceção para caso o arquivo pelo dado path não exista
try:
    # Lendo o arquivo de maneira segura
    with open('alunos.csv') as alunos_csv:
        # Criando um dictionary (dicionario) a partir de alunos_csv
        leitor = csv.DictReader(alunos_csv)
        # Input
        string = str(input("Digite a matricula: "))
        for linha in leitor:
            if(linha['matricula'] == string):
                # Split transforma uma string em um dicionario, com os elementos sendo sequencias de caracter separadas por um char 
                # (split sem argumento separa strings por espaço em branco)
                if linha['status'] == 'Inativo':
                    print("Voce esta Inativo")
                    exit(1)
                dictionary = linha['nome'].split()
                telephone = linha['telefone']
                break

        # Criando as sugestões de email, com um sufixo uff porque o final é sempre o mesmo
        uff = '@id.uff.br'
        email_suggestion1 = dictionary[0]+'.'+dictionary[1]+uff
        email_suggestion2 = dictionary[0]+'.'+dictionary[2]+uff
        email_suggestion3 = dictionary[0][0]+dictionary[2]+uff
        email_suggestion4 = dictionary[0]+'_'+dictionary[1][0]+dictionary[2][0]+uff
        email_suggestion5 = dictionary[0]+dictionary[1]+dictionary[2]+uff

        # Perguntando ao usuario qual nome ele deseja escolher
        print(dictionary[0],",por favor escolha uma das opcoes abaixo para o seu UFFMail")

        print("1 -",email_suggestion1.lower())
        print("2 -",email_suggestion2.lower())
        print("3 -",email_suggestion3.lower())
        print("4 -",email_suggestion4.lower())
        print("5 -",email_suggestion5.lower())

        answer=input()
        
        # IF Statement para ver qual foi a resposta escolhida e mostrar na tela que o email solicitado esta sendo criado, e o sms enviado
        if answer == '1':
            print("A criação do seu email (",email_suggestion1.lower(),") será feita nos próximos minutos")
            print("Um SMS foi enviado para",telephone," com a sua senha de acesso")

        elif answer == '2':
            print("A criação do seu email (",email_suggestion2.lower(),") será feita nos próximos minutos")
            print("Um SMS foi enviado para",telephone," com a sua senha de acesso")

        elif answer == '3':
            print("A criação do seu email (",email_suggestion3.lower(),") será feita nos próximos minutos")
            print("Um SMS foi enviado para",telephone," com a sua senha de acesso")

        elif answer == '4':
            print("A criação do seu email (",email_suggestion4.lower(),") será feita nos próximos minutos")
            print("Um SMS foi enviado para",telephone," com a sua senha de acesso")

        elif answer == '5':
            print("A criação do seu email (",email_suggestion5.lower(),") será feita nos próximos minutos")
            print("Um SMS foi enviado para",telephone," com a sua senha de acesso")


except FileNotFoundError:
    print("Arquivo não existe")
    exit(1)