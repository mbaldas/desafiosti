# Fazendo os imports necessarios (requests, json para acessar a api, geopy para a localizaçao do usuário e math e haversine para o cálculo das distancias)
import requests
import json
from geopy.geocoders import Nominatim
from math import *
from haversine import haversine

# Até a linha 13 ele pega a localização do usuario utilizando a geopy
resposta = input("Digite onde você está:")

geolocator = Nominatim(user_agent="desafio2.py")
location = geolocator.geocode(resposta)
print("Sua latitude e longitude:")
print((location.latitude,location.longitude))

# Dando um GET na api, e transformando o JSON em um objeto python para tratar o dado
response = requests.get('http://dadosabertos.rio.rj.gov.br/apiTransporte/apresentacao/rest/index.cfm/estacoesBikeRio')
json_data = json.loads(response.text)

latitudes = []
longitudes = []

# Jogando os dados do JSON de latitude numa lista
for i in range (0,57):
    latitudes.append(json_data['DATA'][i][5])


# Jogando os dados do JSON de longitude numa lista
for i in range (0,57):
    longitudes.append(json_data['DATA'][i][6])

# Jogando a latitude e longitude do usuario na variavel coord
coord = (location.latitude,location.longitude)

# Transformando as latitudes e longitudes do BikeRio numa tupla, para usar a formula de haversine - tive que fazer manualmente *README.txt*
coord2 = list(zip(latitudes,longitudes))

distance = []

#for i in range (0,57):
    #distance = haversine(coord,coord2[i])

coord4 = haversine(coord,coord2[0])
coord5 = haversine(coord,coord2[1])
coord6 = haversine(coord,coord2[2])
coord7 = haversine(coord,coord2[3])
coord9 = haversine(coord,coord2[5])
coord10 = haversine(coord,coord2[6])
coord11 = haversine(coord,coord2[7])
coord12 = haversine(coord,coord2[8])
coord13 = haversine(coord,coord2[9])
coord14 = haversine(coord,coord2[10])
coord15 = haversine(coord,coord2[11])
coord16 = haversine(coord,coord2[12])
coord17 = haversine(coord,coord2[13])
coord18 = haversine(coord,coord2[14])
coord19 = haversine(coord,coord2[15])
coord20 = haversine(coord,coord2[16])
coord21 = haversine(coord,coord2[17])
coord22 = haversine(coord,coord2[18])
coord23 = haversine(coord,coord2[19])
coord24 = haversine(coord,coord2[20])
coord25 = haversine(coord,coord2[21])
coord26 = haversine(coord,coord2[22])
coord27 = haversine(coord,coord2[23])
coord28 = haversine(coord,coord2[24])
coord29 = haversine(coord,coord2[25])
coord30 = haversine(coord,coord2[26])
coord31 = haversine(coord,coord2[27])
coord32 = haversine(coord,coord2[28])
coord33 = haversine(coord,coord2[29])
coord34 = haversine(coord,coord2[30])
coord35 = haversine(coord,coord2[31])
coord36 = haversine(coord,coord2[32])
coord37 = haversine(coord,coord2[33])
coord38 = haversine(coord,coord2[34])
coord39 = haversine(coord,coord2[35])
coord40 = haversine(coord,coord2[36])
coord41 = haversine(coord,coord2[37])
coord42 = haversine(coord,coord2[38])
coord43 = haversine(coord,coord2[39])
coord44 = haversine(coord,coord2[40])
coord45 = haversine(coord,coord2[41])
coord46 = haversine(coord,coord2[42])
coord47 = haversine(coord,coord2[43])
coord48 = haversine(coord,coord2[44])
coord49 = haversine(coord,coord2[45])
coord50 = haversine(coord,coord2[46])
coord51 = haversine(coord,coord2[47])
coord52 = haversine(coord,coord2[48])
coord53 = haversine(coord,coord2[49])
coord54 = haversine(coord,coord2[50])
coord55 = haversine(coord,coord2[51])
coord56 = haversine(coord,coord2[52])
coord57 = haversine(coord,coord2[53])
coord58 = haversine(coord,coord2[54])
coord59 = haversine(coord,coord2[55])
coord60 = haversine(coord,coord2[56])

distance.append(coord4)
distance.append(coord5)
distance.append(coord6)
distance.append(coord7)
distance.append(coord9)
distance.append(coord10)
distance.append(coord11)
distance.append(coord12)
distance.append(coord13)
distance.append(coord14)
distance.append(coord15)
distance.append(coord16)
distance.append(coord17)
distance.append(coord18)
distance.append(coord19)
distance.append(coord20)
distance.append(coord21)
distance.append(coord22)
distance.append(coord23)
distance.append(coord24)
distance.append(coord25)
distance.append(coord26)
distance.append(coord27)
distance.append(coord28)
distance.append(coord29)
distance.append(coord30)
distance.append(coord31)
distance.append(coord32)
distance.append(coord33)
distance.append(coord34)
distance.append(coord35)
distance.append(coord36)
distance.append(coord37)
distance.append(coord38)
distance.append(coord39)
distance.append(coord40)
distance.append(coord41)
distance.append(coord42)
distance.append(coord43)
distance.append(coord44)
distance.append(coord45)
distance.append(coord46)
distance.append(coord47)
distance.append(coord48)
distance.append(coord49)
distance.append(coord50)
distance.append(coord51)
distance.append(coord52)
distance.append(coord53)
distance.append(coord54)
distance.append(coord55)
distance.append(coord56)
distance.append(coord57)
distance.append(coord58)
distance.append(coord59)
distance.append(coord60)


print("As distancias em km para as estações BikeRio são:")
# Printando da menor para a maior distância
print(sorted(distance, key=int))


